package gui.android.openbd.activities.logged.collections

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import com.viethoa.RecyclerViewFastScroller
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.lists.adapters.collections.AdapterCollectionMy
import gui.android.openbd.apis.events.EventCollection
import com.viethoa.models.AlphabetItem
import gui.android.openbd.activities.logged.resultats.ResultatActivity
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.events.EventSearch
import gui.android.openbd.apis.requests.RequestCollection
import gui.android.openbd.apis.requests.RequestSearch
import gui.android.openbd.tools.CaptureISBNPortrait
import gui.android.openbd.tools.PreferenceHelper.COLLECTION_COUNT
import gui.android.openbd.tools.constants.APP_VISIBILITY_SCROLL_BAR
import java.io.Serializable

/**
 * Created by jeremy on 27/12/17.
 *
 * Function for communicate to sub code in application
 */
class CollectionPresenter:
        LoggedPresenterImpl<CollectionContract.View>(),
        CollectionContract.Presenter {
    override fun initializeRequester() {
        RequestCollection.instance.setActivity(mActivity)
        RequestSearch.instance.setActivity(mActivity)
    }

    fun eventCreateActivity(collectionList: RecyclerView, alphabetList: RecyclerViewFastScroller, progressBar: ProgressBar) {
        val collection = mPref.getString(COLLECTION_COUNT, "0").toInt()
        if (collection >= APP_VISIBILITY_SCROLL_BAR) {
            alphabetList.visibility = View.VISIBLE
            collectionList.setPadding(0, 0, 30, 0)
        } else {
            collectionList.setPadding(0, 0, 0, 0)
        }

        displayProgressBar(progressBar)
    }

    fun eventCollection(event: EventCollection, collectionList: RecyclerView, alphabetList: RecyclerViewFastScroller, progressBar: ProgressBar) {
        completeListCollection(collectionList, event)
        mPref.edit().putString(COLLECTION_COUNT, collectionList.adapter.itemCount.toString()).apply()
        alphabetList.setRecyclerView(collectionList)
        alphabetList.setUpAlphabet(generateIndexScroll(collectionList.adapter as AdapterCollectionMy))

        goneProgressBar(progressBar)
    }

    fun eventSearchSuccess(event: EventSearch, progressBar: ProgressBar) {
        val intent = Intent(mActivity, ResultatActivity::class.java)

        goneProgressBar(progressBar)

        intent.putExtra("resultat", event.getResultat() as Serializable)
        mActivity.startActivity(intent)
    }

    fun eventSearchFailed(event: Event, progressBar: ProgressBar) {
        if (!event.isSuccess) {
            val intent = Intent(mActivity, ResultatActivity::class.java)
            mActivity.startActivity(intent)

            goneProgressBar(progressBar)
        }
    }

    fun eventActivityResult(requestCode: Int, resultCode: Int, progressBar: ProgressBar, data: Intent?): Boolean {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        displayProgressBar(progressBar)

        return if (result != null) {
            if (result.contents == null) {
                Toast.makeText(mActivity, "Cancelled", Toast.LENGTH_LONG).show()
                true
            } else {
                // super.onActivityResult(requestCode, resultCode, data)
                Toast.makeText(mActivity, "Search for ISBN : " + result.contents, Toast.LENGTH_LONG).show()
                requestISBN(result.contents)
                false
            }
        } else {
            // super.onActivityResult(requestCode, resultCode, data)
            false
        }
    }

    fun clickToSearchAlbum() {
        val integrator = IntentIntegrator(mActivity)
        integrator.setOrientationLocked(false)
        integrator.setBeepEnabled(false)
        integrator.captureActivity = CaptureISBNPortrait::class.java
        integrator.initiateScan()
    }

    private fun completeListCollection(list: RecyclerView, event: EventCollection) {
        val adapter = AdapterCollectionMy(event.list, mActivity)
        val layoutManager = LinearLayoutManager(mActivity)

        list.layoutManager = layoutManager
        list.adapter = adapter
    }

    private fun generateIndexScroll(items: AdapterCollectionMy): ArrayList<AlphabetItem> {
        val mAlphabetItems = ArrayList<AlphabetItem>()
        val strAlphabets = ArrayList<String>()

        for (i in 0 until items.itemCount) {
            val item = items.getItem(i)
            val word = item.getSaga().substring(0, 1)

            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word)
                mAlphabetItems.add(AlphabetItem(i, word, i == 0))
            }
        }

        return mAlphabetItems
    }

    private fun displayProgressBar(progressBar: ProgressBar) {
        mActivity.runOnUiThread({
            progressBar.visibility = View.VISIBLE
        })
    }

    private fun goneProgressBar(progressBar: ProgressBar) {
        mActivity.runOnUiThread({
            progressBar.visibility = View.GONE
        })
    }

    private fun requestISBN(search: String) {
        RequestSearch.instance.searchISBN13(search)
    }
}
