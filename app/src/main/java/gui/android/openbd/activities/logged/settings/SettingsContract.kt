package gui.android.openbd.activities.logged.settings

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 18/01/18.
 */
class SettingsContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}