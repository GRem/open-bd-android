package gui.android.openbd.activities.logged.settings

import android.os.Bundle
import butterknife.ButterKnife
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.settings.fragments.SettingsFragment

class SettingsActivity:
        LoggedActivity<SettingsContract.View, SettingsPresenter>(),
    SettingsContract.View {
    override var mPresenter: SettingsPresenter = SettingsPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        ButterKnife.bind(this)

        drawerNavigationBar(this,resources.getString(R.string.activity_settings_title))
    }

    override fun onStart() {
        super.onStart()
        fragmentManager
                .beginTransaction()
                .replace(R.id.activity_settings_fragment, SettingsFragment())
                .commit()
    }
}
