package gui.android.openbd.activities.logged.details

import android.app.Activity
import android.text.TextUtils
import android.view.View.*
import android.widget.TextView
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.apis.models.Book

/**
 * Created by jeremy on 12/01/18.
 *
 * Function for communicate to sub code in application for
 * prepare data and return just element to display
 */
class DetailPresenter:
        LoggedPresenterImpl<DetailContract.View>(),
        DetailContract.Presenter {
    fun setIDBook(vID: TextView, book: Book) {
        vID.text = book.getId()
    }

    fun setSaga(vSaga: TextView, book: Book) {
        var visibility: Int = GONE

        if (book.hasSaga()) {
            visibility = VISIBLE
            vSaga.text = book.getSaga()
        }
        vSaga.visibility = visibility
    }

    fun setTitle(vTitle: TextView, book: Book) {
        vTitle.text = book.getName()
    }

    fun setISBN13(vISBN: TextView, book: Book) {
        var visibility: Int = GONE

        if (book.hasIsbn13()) {
            visibility = VISIBLE
            vISBN.text = mActivity.resources.getString(R.string.activity_detail_text_isbn13, book.getIsbn13())
        }
        vISBN.visibility = visibility
    }

    fun setISBN10(vISBN: TextView, book: Book) {
        var visibility: Int = GONE

        if (book.hasIsbn10()) {
            visibility = VISIBLE
            vISBN.text = mActivity.resources.getString(R.string.activity_detail_text_isbn13, book.getIsbn10())
        }
        vISBN.visibility = visibility
    }

    fun setDescription(vDescribe: TextView, book: Book) {
        vDescribe.text = book.getDescription()
    }

    fun setAuthors(vAuthors: TextView, book: Book) {
        var visibility: Int = INVISIBLE

        if (book.hasAuthors()) {
            visibility = VISIBLE
            vAuthors.text = TextUtils.join(", ", book.getAuthors())
        }
        vAuthors.visibility = visibility
    }

    fun setCategories(vCategory: TextView, book: Book) {
        var visibility: Int = INVISIBLE

        if (book.hasCategories()) {
            visibility = VISIBLE
            vCategory.text = TextUtils.join(", ", book.getCategories())
        }

        vCategory.visibility = visibility
    }
}
