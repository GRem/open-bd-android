package gui.android.openbd.activities.base

import android.content.Context
import android.support.annotation.StringRes

/**
 * Created by jeremy on 27/12/17.
 *
 * Interface used for view in activities
 */
interface BaseView {
    fun getContext(): Context
    fun showError(error: String?)
    fun showError(@StringRes stringResId: Int)
    fun showMessage(@StringRes srtResId: Int)
    fun showMessage(message: String)
}