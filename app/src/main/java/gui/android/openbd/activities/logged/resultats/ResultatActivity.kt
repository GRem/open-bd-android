package gui.android.openbd.activities.logged.resultats

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.collections.CollectionActivity
import gui.android.openbd.apis.models.Search
import gui.android.openbd.apis.requests.RequestUser
import gui.android.openbd.tools.constants.SEARCH_FAILED
import gui.android.openbd.tools.constants.SEARCH_PRESENT
import gui.android.openbd.tools.constants.SEARCH_SUCCESS
import kotterknife.bindView

/**
 * Created by jeremy on 17/01/18.
 *
 * Display result to ISBN searched
 */
class ResultatActivity:
        LoggedActivity<ResultatContract.View, ResultatPresenter>(),
        ResultatContract.View {
    private val mData: LinearLayout by bindView(R.id.activity_resultat_data)
    private val mTitle: TextView by bindView(R.id.activity_resultat_title)
    private val mYear: TextView by bindView(R.id.activity_resultat_year)
    private val mISBN: TextView by bindView(R.id.activity_resultat_isbn)
    private val mButton: Button by bindView(R.id.activity_resultat_add)
    private var status: String = SEARCH_SUCCESS
    override var mPresenter: ResultatPresenter = ResultatPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultat)
        ButterKnife.bind(this)

        RequestUser.instance.setActivity(this)
        drawerNavigationBar(this,resources.getString(R.string.activity_resultat_title))
    }

    override fun onResume() {
        super.onResume()
        if (intent.extras == null) {
            searchFailed()
        } else {
            searchSuccess()
        }
    }

    override fun onStop() {
        super.onStop()
        finish()
    }

    private fun searchFailed() {
        mData.visibility = View.GONE
        mButton.text = resources.getString(R.string.activity_resultat_not_found)
        mButton.setBackgroundColor(getColor(R.color.gray))

        status = SEARCH_FAILED
    }

    private fun searchSuccess() {
        val resultat = intent.extras.get("resultat") as Search

        mTitle.text = resultat.getTitle()
        mYear.text = resultat.getYear()
        mISBN.text = resultat.getIsbn13()

        mButton.setBackgroundColor(getColor(R.color.green))

        status = SEARCH_SUCCESS
    }

    @OnClick(R.id.activity_resultat_add)
    fun onClickButtonAddToCollection() {
        when(status) {
            SEARCH_FAILED -> actionBack()
            SEARCH_SUCCESS -> actionAdd()
            SEARCH_PRESENT -> actionAlready()
        }
    }

    private fun actionBack() {
        startActivity(Intent(this, CollectionActivity::class.java))
    }

    private fun actionAdd() {
        val resultat = intent.extras.get("resultat") as Search

        RequestUser.instance.addBokToCollection(resultat.getBookId())
        startActivity(Intent(this, CollectionActivity::class.java))
    }

    private fun actionAlready() {
        startActivity(Intent(this, CollectionActivity::class.java))
    }
}