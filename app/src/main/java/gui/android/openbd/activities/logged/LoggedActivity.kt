package gui.android.openbd.activities.logged

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badge
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import co.zsmb.materialdrawerkt.imageloader.drawerImageLoader
import gui.android.openbd.activities.base.BaseActivity
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.mikepenz.materialdrawer.util.DrawerUIUtils
import com.squareup.picasso.Picasso
import gui.android.openbd.R
import gui.android.openbd.activities.base.logins.LoginActivity
import gui.android.openbd.activities.logged.collections.CollectionActivity
import gui.android.openbd.activities.logged.friends.FriendActivity
import gui.android.openbd.activities.logged.settings.SettingsActivity
import gui.android.openbd.activities.logged.users.UserActivity
import gui.android.openbd.apis.requests.RequestSign
import gui.android.openbd.tools.DownloadGravatar
import gui.android.openbd.tools.PreferenceHelper
import gui.android.openbd.tools.PreferenceHelper.ACTIVITY_SELECTED
import gui.android.openbd.tools.PreferenceHelper.COLLECTION_COUNT
import gui.android.openbd.tools.PreferenceHelper.EMAIL
import gui.android.openbd.tools.PreferenceHelper.FRIEND_COUNT
import gui.android.openbd.tools.PreferenceHelper.PSEUDO
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import kotlin.reflect.KClass

/**
 * Created by jeremy on 15/01/18.
 *
 * Base for all activity when user is logged to service
 */
abstract class LoggedActivity<in V : LoggedView, T : LoggedPresenter<V>>
    : BaseActivity<V, T>(), LoggedView {
    private lateinit var vToolbar: Toolbar
    private lateinit var vDrawer: Drawer
    private lateinit var mActivity: Activity

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        mPresenter.initializeRequester()
    }
    /**
     * Call this method when create activity
     */
    fun drawerNavigationBar(activity: AppCompatActivity, titleBar: String) {
        mActivity = activity
        vToolbar = activity.findViewById(R.id.activity_logged_toolbar)
        activity.setSupportActionBar(vToolbar)
        val prefs = defaultPrefs(activity)
        val email = prefs.getString(EMAIL, "")
        val pseudo = prefs.getString(PSEUDO, "")

        vDrawer = drawer {
            drawerImageLoader {
                placeholder { ctx, _ ->
                    DrawerUIUtils.getPlaceHolder(ctx)
                }
                set { imageView, uri, placeholder, _ ->
                    Picasso.get()
                            .load(uri)
                            .placeholder(placeholder!!)
                            .into(imageView)
                }
                cancel { imageView ->
                    Picasso.get()
                            .cancelRequest(imageView)
                }
            }

            accountHeader {
                backgroundDrawable = resources.getDrawable(R.drawable.header_user_android, null)
                profile(pseudo, email) {
                    iconUrl = DownloadGravatar.instance.gravatarImageUrl(email)
                }
                onProfileImageClick { _, _, _ ->
                    startActivity(Intent(this@LoggedActivity, UserActivity::class.java))
                    false
                }
                onSelectionViewClick { _, _ ->
                    startActivity(Intent(this@LoggedActivity, UserActivity::class.java))
                    false
                }
                selectionListEnabledForSingleProfile = true
                textColorRes = R.color.black
                selectionListEnabled = false
            }

            primaryItem(R.string.activity_collection_title) {
                icon = R.drawable.ic_collection
                textColorRes = R.color.black
                iconColorRes = R.color.black
                badge(prefs.getString(COLLECTION_COUNT, "0")) {
                    cornersDp = 0
                    color = 0xFF0099FF
                    colorPressed = 0xFFCC99FF
                }

                onClick(openActivity(CollectionActivity::class, prefs))
            }

            primaryItem(R.string.activity_friend_title) {
                icon = R.drawable.ic_friends
                textColorRes = R.color.black
                iconColorRes = R.color.black
                badge(prefs.getString(FRIEND_COUNT, "0")) {
                    cornersDp = 0
                    color = 0xFF0099FF
                    colorPressed = 0xFFCC99FF
                }

                onClick(openActivity(FriendActivity::class, prefs))
            }

            divider {}
            primaryItem(R.string.activity_settings_title) {
                icon = R.drawable.ic_settings
                textColorRes = R.color.black
                iconColorRes = R.color.black

                onClick(openActivity(SettingsActivity::class, prefs))
            }

            primaryItem(R.string.menu_disconnect) {
                icon = R.drawable.ic_logout
                textColorRes = R.color.black
                iconColorRes = R.color.black

                onClick(closeSession(LoginActivity::class))
            }

            selectedItemByPosition = prefs.getInt(ACTIVITY_SELECTED, 0)
            closeOnClick = true
            sliderBackgroundColorRes = R.color.colorPrimaryDark
        }

        vToolbar.setNavigationOnClickListener { _ ->
            vDrawer.openDrawer()
        }

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = titleBar
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayUseLogoEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu)
        }
    }

    /**
     * Use back button in toolbar
     */
    @SuppressLint("PrivateResource")
    fun <T : Activity> drawerNavigationBack(activity: AppCompatActivity, titleBar: String, backActivity: KClass<T>) {
        mActivity = activity
        vToolbar = activity.findViewById(R.id.activity_logged_toolbar)
        activity.setSupportActionBar(vToolbar)

        val actionBar = supportActionBar

        vToolbar.setNavigationOnClickListener { _ ->
            startActivity(Intent(this@LoggedActivity, backActivity.java))
        }

        if (actionBar != null) {
            actionBar.title = titleBar

            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayUseLogoEnabled(true)

            actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material)
        }
    }

    private fun <T : Activity> openActivity(activity: KClass<T>, prefs: SharedPreferences):
            (View?, Int?, IDrawerItem<*,*>?) -> Boolean = { _: View?, position: Int?, _: IDrawerItem<*, *>? ->
        Log.d("SELECT >>", "New position --> $position")
        prefs.edit().putInt(ACTIVITY_SELECTED, position!!).apply()
        startActivity(Intent(this@LoggedActivity, activity.java))
        false
    }

    private fun <T : Activity>closeSession(activity: KClass<T>): (View?) -> Boolean = {
        RequestSign.instance.setActivity(this)
        RequestSign.instance.signDown()

        intent = Intent(this@LoggedActivity, activity.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

        PreferenceHelper.cleanProperties(this)
        finish()

        false
    }
}
