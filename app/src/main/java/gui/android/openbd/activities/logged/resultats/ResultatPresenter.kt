package gui.android.openbd.activities.logged.resultats

import gui.android.openbd.activities.logged.LoggedPresenterImpl

/**
 * Created by jeremy on 17/01/18.
 */
class ResultatPresenter:
        LoggedPresenterImpl<ResultatContract.View>(),
        ResultatContract.Presenter