package gui.android.openbd.activities.logged.friends.search

import android.app.Activity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.lists.adapters.AdapterFriend
import gui.android.openbd.apis.events.EventFriends
import gui.android.openbd.apis.requests.RequestFriend

/**
 * Created by jeremy on 25/02/18.
 *
 * Manipulate data
 */
class SearchPresenter:
        LoggedPresenterImpl<SearchContract.View>(),
        SearchContract.Presenter {
    fun completeListSearchFriend(list: RecyclerView, event: EventFriends) {
        val adapter = AdapterFriend(event.list!!.toList(), mActivity)
        val layoutManager = LinearLayoutManager(mActivity)

        list.layoutManager = layoutManager
        list.adapter = adapter
    }
}