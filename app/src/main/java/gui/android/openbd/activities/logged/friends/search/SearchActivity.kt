package gui.android.openbd.activities.logged.friends.search

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.thedeadpixelsociety.passport.notEmpty
import com.thedeadpixelsociety.passport.passport
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.friends.FriendActivity
import gui.android.openbd.apis.events.EventFriends
import gui.android.openbd.apis.requests.RequestFriend
import gui.android.openbd.apis.requests.RequestSearch
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by jeremy on 25/02/18.
 *
 * Activity for search friend in service
 */
class SearchActivity:
        LoggedActivity<SearchContract.View, SearchPresenter>(),
        SearchContract.View {
    private val mProgress: ProgressBar by bindView(R.id.activity_progress_bar)
    private val mSearch: EditText by bindView(R.id.activity_friend_search_text)
    private val mResultEmpty: TextView by bindView(R.id.activity_friend_search_no_result)
    private val mFriendList: RecyclerView by bindView(R.id.layout_friend_list_view)
    override var mPresenter: SearchPresenter = SearchPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend_search)
        ButterKnife.bind(this)

        mPresenter.initialize(this)

        RequestFriend.instance.setActivity(this)

        drawerNavigationBack(this, resources.getString(R.string.activity_search_friend_title), FriendActivity::class)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        RequestSearch.instance.setActivity(this)

        val forgot: String = resources.getString(R.string.activity_search_friend_no_resultat)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            @RequiresApi(Build.VERSION_CODES.N)
            mResultEmpty.text = Html.fromHtml(forgot, Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE)
        } else {
            @Suppress("DEPRECATION")
            mResultEmpty.text = Html.fromHtml(forgot)
        }
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @OnClick(R.id.activity_friend_search_submit)
    fun onClickSearchFriend() {
        if (validator.validate(mSearch)) {
            mProgress.visibility = View.VISIBLE
            mResultEmpty.visibility = View.GONE
            RequestSearch.instance.searchFriends(mSearch.text.toString())
        }
    }

    @Subscribe
    fun onEventSearchFriend(event: EventFriends) {
        mProgress.visibility = View.GONE

        if (event.isSuccess) {
            mPresenter.completeListSearchFriend(mFriendList, event)
        } else {
            mResultEmpty.visibility = View.VISIBLE
        }
    }

    private val validator by lazy {
        passport {
            rules<String?>(mSearch) {
                notEmpty()
            }
        }
    }
}