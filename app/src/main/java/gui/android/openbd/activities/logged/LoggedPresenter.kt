package gui.android.openbd.activities.logged

import gui.android.openbd.activities.base.BasePresenter

/**
 * Created by jeremy on 15/01/18.
 *
 * Manipulate data in application
 */
interface LoggedPresenter<in V : LoggedView> : BasePresenter<V> {
    fun initializeRequester()
}