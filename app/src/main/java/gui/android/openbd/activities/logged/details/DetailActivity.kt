package gui.android.openbd.activities.logged.details

import android.content.Intent
import android.os.Bundle
import android.text.style.ImageSpan
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.collections.CollectionActivity
import gui.android.openbd.apis.events.EventBook
import gui.android.openbd.apis.events.EventRemove
import gui.android.openbd.apis.requests.RequestBook
import gui.android.openbd.apis.requests.RequestUser
import gui.android.openbd.tools.DownloadImage
import gui.android.openbd.tools.PreferenceHelper.BOOK_ID
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import gui.android.openbd.tools.constants.API_URL_SIMPLE
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by jeremy on 12/01/18.
 *
 * Display a detail to item collection
 */
class DetailActivity :
        LoggedActivity<DetailContract.View, DetailPresenter>(),
        DetailContract.View {
    private val vId: TextView by bindView(R.id.activity_detail_id)
    private val vName: TextView by bindView(R.id.activity_detail_name)
    private val vSaga: TextView by bindView(R.id.activity_detail_saga)
    private val vIsbn13: TextView by bindView(R.id.activity_detail_isbn13)
    private val vIsbn10: TextView by bindView(R.id.activity_detail_isbn10)
    private val vDescription: TextView by bindView(R.id.activity_detail_description_text)
    private val vCover: ImageView by bindView(R.id.activity_detail_description_cover)
    private val vAuthors: TextView by bindView(R.id.activity_detail_author)
    private val vCategories: TextView by bindView(R.id.activity_detail_category)
    override var mPresenter: DetailPresenter = DetailPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        ButterKnife.bind(this)

        mPresenter.initialize(this)

        RequestBook.instance.setActivity(this)
        RequestUser.instance.setActivity(this)
        drawerNavigationBack(this,
                resources.getString(R.string.activity_detail_title),
                CollectionActivity::class)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        val prefs = defaultPrefs(this)
        RequestBook.instance.showBook(prefs.getString(BOOK_ID, ""))
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEventBook(event: EventBook) {
        if (event.book.hasCover()) {
            val dl = DownloadImage(vCover)
            dl.execute(API_URL_SIMPLE + event.book.getCoverNormal())
            ImageSpan(this, dl.get())
        }

        if (event.book.hasCover()) {
            val dl = DownloadImage(vCover)
            dl.execute(API_URL_SIMPLE + event.book.getCoverNormal())
            vCover.setImageBitmap(dl.get())
        }

        vName.text = event.book.getName()

        mPresenter.setIDBook(vId, event.book)
        mPresenter.setSaga(vSaga, event.book)
        mPresenter.setTitle(vName, event.book)
        mPresenter.setISBN13(vIsbn13, event.book)
        mPresenter.setISBN10(vIsbn10, event.book)
        mPresenter.setDescription(vDescription, event.book)
        mPresenter.setAuthors(vAuthors, event.book)
        mPresenter.setCategories(vCategories, event.book)
    }

    @Subscribe
    fun onEventRemove(event: EventRemove) {
        if (event.isSuccess) {
            Toast.makeText(this, getString(R.string.activity_detail_remove_success), Toast.LENGTH_LONG).show()
            startActivity(Intent(this@DetailActivity, CollectionActivity::class.java))
            finish()
        } else {
            Toast.makeText(this, getString(R.string.activity_detail_remove_failed), Toast.LENGTH_LONG).show()
        }
    }

    @OnClick(R.id.activity_detail_remove_album)
    fun onClickRemoveAlbum() {
        RequestUser.instance.removeBookToCollection(vId.text.toString())
    }
}
