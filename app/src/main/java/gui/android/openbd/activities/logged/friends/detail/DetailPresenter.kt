package gui.android.openbd.activities.logged.friends.detail

import android.app.Activity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.viethoa.models.AlphabetItem
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.lists.adapters.collections.AdapterCollectionMy
import gui.android.openbd.apis.events.EventCollection
import gui.android.openbd.apis.requests.RequestFriend
import gui.android.openbd.lists.adapters.collections.AdapterCollectionFriend

/**
 * Created by jeremy on 11/03/18.
 */
class DetailPresenter:
        LoggedPresenterImpl<DetailContract.View>(),
        DetailContract.Presenter {
    fun completeListCollection(list: RecyclerView, event: EventCollection) {
        val adapter = AdapterCollectionFriend(event.list, mActivity)
        val layoutManager = LinearLayoutManager(mActivity)

        list.layoutManager = layoutManager
        list.adapter = adapter
    }

    fun generateIndexScroll(items: AdapterCollectionMy): ArrayList<AlphabetItem> {
        val mAlphabetItems = ArrayList<AlphabetItem>()
        val strAlphabets = ArrayList<String>()

        for (i in 0 until items.itemCount) {
            val item = items.getItem(i)
            val word = item.getSaga().substring(0, 1)

            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word)
                mAlphabetItems.add(AlphabetItem(i, word, i == 0))
            }
        }

        return mAlphabetItems
    }
}