package gui.android.openbd.activities.logged

import gui.android.openbd.activities.base.BaseView

/**
 * Created by jeremy on 15/01/18.
 *
 * Interface used for view in activities when user is logged
 */
interface LoggedView : BaseView
