package gui.android.openbd.activities.logged.users

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 26/02/18.
 */
class UserContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}