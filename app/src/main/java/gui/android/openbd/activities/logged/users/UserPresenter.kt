package gui.android.openbd.activities.logged.users

import android.app.Activity
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.apis.requests.RequestStat
import gui.android.openbd.tools.PreferenceHelper.EMAIL
import gui.android.openbd.tools.PreferenceHelper.PSEUDO
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs

/**
 * Created by jeremy on 26/02/18.
 *
 * Manipulate user data
 */
class UserPresenter:
        LoggedPresenterImpl<UserContract.View>(),
        UserContract.Presenter {
    fun getUserValueInformation() {
        // mActivity.vEmail.text = mPref.getString(EMAIL, "")
        // mActivity.vPseudo.text = mPref.getString(PSEUDO, "")
    }
}