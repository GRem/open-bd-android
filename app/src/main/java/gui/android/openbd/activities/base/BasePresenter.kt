package gui.android.openbd.activities.base

/**
 * Created by jeremy on 27/12/17.
 *
 * Interface for presenters used in activities
 */
interface BasePresenter<in V : BaseView> {
    fun attachView(view: V)
    fun detachView()
}