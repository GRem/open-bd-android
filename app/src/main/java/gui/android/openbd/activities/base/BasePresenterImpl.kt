package gui.android.openbd.activities.base

/**
 * Created by jeremy on 27/12/17.
 *
 * Base function for presenters classes in all activities
 */
open class BasePresenterImpl<V : BaseView> : BasePresenter<V> {
    protected var mView: V? = null

    override fun attachView(view: V) {
        mView = view
    }

    override fun detachView() {
        mView = null
    }
}