package gui.android.openbd.activities.logged.collections

import android.os.Bundle
import butterknife.OnClick
import gui.android.openbd.R
import gui.android.openbd.apis.events.EventCollection
import gui.android.openbd.apis.requests.RequestCollection

import kotterknife.bindView
import org.greenrobot.eventbus.Subscribe
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import butterknife.ButterKnife
import com.viethoa.RecyclerViewFastScroller
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.events.EventSearch
import org.greenrobot.eventbus.EventBus

/**
 * Activity for manage collection to user.
 */
class CollectionActivity :
        LoggedActivity<CollectionContract.View, CollectionPresenter>(),
        CollectionContract.View {
    private val mAlphabetList: RecyclerViewFastScroller by bindView(R.id.layout_collection_list_alphabet)
    private val mCollectionList: RecyclerView by bindView(R.id.layout_collection_list_view)
    private val mProgress: ProgressBar by bindView(R.id.activity_progress_bar)
    override var mPresenter: CollectionPresenter = CollectionPresenter()

    /**
     * Create activity. Initialize requester and element in view.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPresenter.initialize(this)

        setContentView(R.layout.activity_collection)
        ButterKnife.bind(this)

        drawerNavigationBar(this,resources.getString(R.string.activity_collection_title))

        mPresenter.eventCreateActivity(mCollectionList, mAlphabetList, mProgress)
    }

    /**
     * Start activity. Register event to view.
     */
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    /**
     * Resume activity. Send request fetching collection data.
     */
    override fun onResume() {
        super.onResume()
        RequestCollection.instance.fetchCollections()
    }

    /**
     * Stop activity. Unregister event to view.
     */
    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_collection, menu)
        return true
    }

    /**
     * Event when collection request is successfully response.
     */
    @Subscribe
    fun onEventCollection(event: EventCollection) {
        mPresenter.eventCollection(event, mCollectionList, mAlphabetList, mProgress)
    }

    /**
     * Event when search book request is successfully response.
     */
    @Subscribe
    fun onEventSearchSuccess(event: EventSearch) {
        mPresenter.eventSearchSuccess(event, mProgress)
    }

    /**
     * Event when search book request is error response.
     */
    @Subscribe
    fun onEventSearchFailed(event: Event) {
        mPresenter.eventSearchFailed(event, mProgress)
    }
    /**
     * Event when user click in action bar menu.
     * * Add : Open activity for scan ISBN in book.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.action_collection_add -> {
                mPresenter.clickToSearchAlbum()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mPresenter.eventActivityResult(requestCode, resultCode, mProgress, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
