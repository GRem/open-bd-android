package gui.android.openbd.activities.base.registers

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.thedeadpixelsociety.passport.*
import gui.android.openbd.R
import gui.android.openbd.activities.base.logins.LoginActivity
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.requests.RequestSign
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import android.view.inputmethod.InputMethodManager
import gui.android.openbd.BuildConfig

class RegisterActivity : AppCompatActivity() {
    private val mEmail: TextView by bindView(R.id.activity_register_email)
    private val mPseudo: TextView by bindView(R.id.activity_register_pseudo)
    private val mPassword: TextView by bindView(R.id.activity_register_password)
    private val mPassword2: TextView by bindView(R.id.activity_register_password2)
    private val mSuccess: TextView by bindView(R.id.activity_register_success_info)
    private val mForgot: TextView by bindView(R.id.activity_sign_in_link_password_forgot)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_register)
        ButterKnife.bind(this)

        RequestSign.instance.setActivity(this)

        val forgot: String = resources.getString(R.string.activity_sign_in_link_forgot_password).format(BuildConfig.WEB)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            @RequiresApi(Build.VERSION_CODES.N)
            mForgot.text = Html.fromHtml(forgot, Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE)
        } else {
            mForgot.text = Html.fromHtml(forgot)
        }

        mForgot.movementMethod = LinkMovementMethod.getInstance()
        mEmail.text = intent.getStringExtra("email")
        mPassword.text = intent.getStringExtra("password")
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @OnClick(R.id.activity_register_sign_up)
    fun onClickRegister() {
        if (validator.validate(mEmail) &&
                validator.validate(mPassword) &&
                validator.validate(mPassword2)) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(mEmail.windowToken, 0)
            imm.hideSoftInputFromWindow(mPassword.windowToken, 0)
            imm.hideSoftInputFromWindow(mPassword2.windowToken, 0)

            RequestSign.instance.signUp(mEmail.text.toString(),
                    mPseudo.text.toString(),
                    mPassword.text.toString(),
                    mPassword2.text.toString())
        }
    }

    @Subscribe
    fun onEventSignUp(event: Event) {
        if (event.isSuccess) {
            Toast.makeText(this, R.string.activity_register_success, Toast.LENGTH_LONG).show()

            mSuccess.visibility = View.VISIBLE
            mSuccess.setOnClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        } else {
            Toast.makeText(this, R.string.activity_register_failed, Toast.LENGTH_LONG).show()
        }
    }

    private val validator by lazy {
        passport {
            rules<String?>(mEmail) {
                email()
                notEmpty()
            }

            rules<String?>(mPseudo) {
                notEmpty()
            }

            rules<String?>(mPassword) {
                matches(mPassword2.text.toString())
                notEmpty()
                length(6, 32)
            }

            rules<String?>(mPassword2) {
                matches(mPassword.text.toString())
                notEmpty()
                length(6, 32)
            }
        }
    }
}
