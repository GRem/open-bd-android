package gui.android.openbd.activities.logged

import android.app.Activity
import android.content.SharedPreferences
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs

/**
 * Created by jeremy on 15/01/18.
 *
 *  Base function for presenters class in all activities when user logged
 */
open class LoggedPresenterImpl<in V: LoggedView> : LoggedPresenter<V> {
    private var mView: V? = null
    protected lateinit var mActivity: Activity
    protected lateinit var mPref: SharedPreferences

    fun initialize(activity: Activity) {
        mActivity = activity
        mPref = defaultPrefs(mActivity)
    }

    override fun initializeRequester() {
    }

    override fun attachView(view: V) {
        mView = view
    }

    override fun detachView() {
        mView = null
    }
}