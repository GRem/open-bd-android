package gui.android.openbd.activities.logged.friends

import android.app.Activity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import gui.android.openbd.activities.logged.LoggedPresenterImpl
import gui.android.openbd.lists.adapters.AdapterFriend
import gui.android.openbd.apis.events.EventFriend
import gui.android.openbd.apis.requests.RequestFriend

/**
 * Created by jeremy on 25/02/18.
 *
 * Prepare data for FriendActivity
 */
class FriendPresenter:
        LoggedPresenterImpl<FriendContract.View>(),
        FriendContract.Presenter {
    fun completeListFriend(list: RecyclerView, event: EventFriend, activity: Activity) {
        val adapter = AdapterFriend(event.getListFriend()!!, activity)
        val layoutManager = LinearLayoutManager(activity)

        list.layoutManager = layoutManager
        list.adapter = adapter
    }
}