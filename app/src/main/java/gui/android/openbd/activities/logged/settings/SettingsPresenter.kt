package gui.android.openbd.activities.logged.settings

import gui.android.openbd.activities.logged.LoggedPresenterImpl

/**
 * Created by jeremy on 18/01/18.
 *
 * Prepare data for this activity
 */
class SettingsPresenter:
        LoggedPresenterImpl<SettingsContract.View>(),
        SettingsContract.Presenter