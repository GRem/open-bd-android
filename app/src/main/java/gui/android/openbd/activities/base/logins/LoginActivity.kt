package gui.android.openbd.activities.base.logins

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.CheckBox
import android.widget.TextView

import butterknife.ButterKnife
import butterknife.OnClick
import com.thedeadpixelsociety.passport.*
import gui.android.openbd.BuildConfig
import gui.android.openbd.R
import gui.android.openbd.activities.logged.collections.CollectionActivity
import gui.android.openbd.activities.base.registers.RegisterActivity
import gui.android.openbd.apis.events.EventSignIn
import gui.android.openbd.apis.requests.RequestSign
import gui.android.openbd.tools.PreferenceHelper.EMAIL
import gui.android.openbd.tools.PreferenceHelper.PASSWORD
import gui.android.openbd.tools.PreferenceHelper.PSEUDO
import gui.android.openbd.tools.PreferenceHelper.TOKEN
import gui.android.openbd.tools.PreferenceHelper.TOKEN_REFRESH
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs

import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity() {
    private val mEmail: TextView by bindView(R.id.activity_sign_in_field_text_email)
    private val mPassword: TextView by bindView(R.id.activity_sign_in_field_text_password)
    private val mRemember: CheckBox by bindView(R.id.activity_sign_in_checkbox_remember)
    private val mForgot: TextView by bindView(R.id.activity_sign_in_link_password_forgot)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)

        RequestSign.instance.setActivity(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        val prefs = defaultPrefs(this)
        val email: String = prefs.getString(EMAIL, "")
        val password: String = prefs.getString(PASSWORD, "")
        val forgot: String = resources.getString(R.string.activity_sign_in_link_forgot_password).format(BuildConfig.WEB)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            @RequiresApi(Build.VERSION_CODES.N)
            mForgot.text = Html.fromHtml(forgot, Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE)
        } else {
            mForgot.text = Html.fromHtml(forgot)
        }
        mForgot.movementMethod = LinkMovementMethod.getInstance()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            mRemember.isChecked = true
            mEmail.text = email
            mPassword.text = password
            onClickSubmit()
        }
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @OnClick(R.id.activity_sign_in_button_sign_in)
    fun onClickSubmit() {
        if (validator.validate(mEmail) && validator.validate(mPassword)) {
            RequestSign.instance.singIn(mEmail.text.toString(), mPassword.text.toString())
        }
    }

    @Subscribe
    fun onEventSignIn(event: EventSignIn) {
        if (event.isSuccess) {
            val prefs = defaultPrefs(this)
            val token = event.readToken()
            val refresh = event.readRefreshToken()
            prefs.edit()
                    .putString(TOKEN, token)
                    .putString(TOKEN_REFRESH, refresh).apply()

            if (mRemember.isChecked) {
                prefs.edit().putString(EMAIL, mEmail.text.toString()).apply()
                prefs.edit().putString(PASSWORD, mPassword.text.toString()).apply()
                prefs.edit().putString(PSEUDO, event.readPseudo()).apply()
            }
            startActivity(Intent(this, CollectionActivity::class.java))
        } else {
            val intent = Intent(this, RegisterActivity::class.java)

            intent.putExtra("email", mEmail.text.toString())
                    .putExtra("password", mPassword.text.toString())

            startActivity(intent)
        }
    }

    private val validator by lazy {
        passport {
            rules<String?>(mEmail) {
                email()
            }

            rules<String?>(mPassword) {
                length(6, 32)
            }
        }
    }
}
