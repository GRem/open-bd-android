package gui.android.openbd.activities.logged.settings.fragments

import android.os.Bundle
import android.preference.PreferenceFragment
import gui.android.openbd.R
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs

/**
 * Created by jeremy on 27/01/18.
 *
 * Display all option to application
 */
class SettingsFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.settings_content)
    }

    override fun onDestroy() {
        super.onDestroy()

        val prefs = defaultPrefs(activity)

        prefs.edit()
                .apply()
    }
}