package gui.android.openbd.activities.logged.collections

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 27/12/17.
 */
object CollectionContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}