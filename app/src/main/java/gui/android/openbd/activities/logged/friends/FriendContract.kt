package gui.android.openbd.activities.logged.friends

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 25/02/18.
 */
class FriendContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}