package gui.android.openbd.activities.logged.resultats

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 17/01/18.
 */
object ResultatContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}