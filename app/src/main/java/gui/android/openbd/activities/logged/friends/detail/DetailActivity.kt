package gui.android.openbd.activities.logged.friends.detail

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.viethoa.RecyclerViewFastScroller
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.friends.FriendActivity
import gui.android.openbd.apis.events.Event
import gui.android.openbd.lists.adapters.collections.AdapterCollectionMy
import gui.android.openbd.apis.events.EventCollection
import gui.android.openbd.apis.requests.RequestFriend
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by jeremy on 11/03/18.
 *
 *
 */
class DetailActivity:
        LoggedActivity<DetailContract.View, DetailPresenter>(),
        DetailContract.View {
    private val mDetailId: TextView by bindView(R.id.itemFriendId)
    private val mDetailPseudo: TextView by bindView(R.id.itemFriendPseudo)
    private val mDetailEmail: TextView by bindView(R.id.itemFriendEmail)
    private val mCollectionList: RecyclerView by bindView(R.id.layout_collection_list_view)
    private val mAlphabetList: RecyclerViewFastScroller by bindView(R.id.layout_collection_list_alphabet)
    override var mPresenter: DetailPresenter = DetailPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend_detail)
        ButterKnife.bind(this)

        drawerNavigationBar(this, resources.getString(R.string.activity_user_friend_search_title))

        mPresenter.initialize(this)

        RequestFriend.instance.setActivity(this)
    }

    override fun onStart() {
        super.onStart()

        EventBus.getDefault().register(this)
        mAlphabetList.visibility = View.GONE

        mDetailId.text = intent.extras.getString("id")
        mDetailEmail.text = intent.extras.getString("email")
        mDetailPseudo.text = intent.extras.getString("pseudo")

        RequestFriend.instance.fetchCollectionToUser(mDetailId.text.toString())
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)

        super.onDestroy()
    }

    @OnClick(R.id.activity_detail_friend_add)
    fun onClickAddFriend() {
        RequestFriend.instance.addNewFriend(mDetailId.text.toString())
    }

    @Subscribe
    fun onEventCollection(event: EventCollection) {
        if (event.isSuccess) {
            Log.d("EVENT", "Success")
            mPresenter.completeListCollection(mCollectionList, event)
            mAlphabetList.setRecyclerView(mCollectionList)
            mAlphabetList.setUpAlphabet(mPresenter.generateIndexScroll(mCollectionList.adapter as AdapterCollectionMy))
        } else {
            Log.d("EVENT", "Failed")
        }
    }

    @Subscribe
    fun onEventAddFriend(event: Event) {
        if (event.isSuccess) {
            // val intent = Intent(this, FriendActivity::class.java)
            // startActivity(intent)
        }
    }
}