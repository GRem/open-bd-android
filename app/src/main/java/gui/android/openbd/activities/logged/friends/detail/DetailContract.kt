package gui.android.openbd.activities.logged.friends.detail

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 11/03/18.
 */
class DetailContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}