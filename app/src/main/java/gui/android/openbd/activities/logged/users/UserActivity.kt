package gui.android.openbd.activities.logged.users

import android.os.Bundle
import android.widget.TextView
import butterknife.ButterKnife
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.collections.CollectionActivity
import gui.android.openbd.apis.events.EventStat
import gui.android.openbd.apis.requests.RequestStat
import gui.android.openbd.tools.PreferenceHelper.EMAIL
import gui.android.openbd.tools.PreferenceHelper.PSEUDO
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by jeremy on 26/02/18.
 *
 * Activity for edit your account
 */
class UserActivity:
        LoggedActivity<UserContract.View, UserPresenter>(),
        UserContract.View {
    val vEmail: TextView by bindView(R.id.activity_user_field_email)
    val vPseudo: TextView by bindView(R.id.activity_user_field_pseudo)
    private val vBooks: TextView by bindView(R.id.activity_user_stat_books)
    private val vPrice: TextView by bindView(R.id.activity_user_stat_price)
    override var mPresenter: UserPresenter = UserPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        ButterKnife.bind(this)

        mPresenter.initialize(this)

        drawerNavigationBack(this, resources.getString(R.string.activity_user_title), CollectionActivity::class)

        RequestStat.instance.setActivity(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        RequestStat.instance.fetchStatUser()
        mPresenter.getUserValueInformation()

        vBooks.text = resources.getString(R.string.activity_user_stat_books, 0.toString())
        vPrice.text = resources.getString(R.string.activity_user_stat_price, 0.toString())
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEventStat(event: EventStat) {
        if (event.isSuccess) {
            vBooks.text = resources.getString(R.string.activity_user_stat_books, event.getBookNumber().toString())
            vPrice.text = resources.getString(R.string.activity_user_stat_price, event.getBookPrice().toString())
        }
    }
}