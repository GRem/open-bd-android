package gui.android.openbd.activities.logged.details

import gui.android.openbd.activities.logged.LoggedPresenter
import gui.android.openbd.activities.logged.LoggedView

/**
 * Created by jeremy on 12/01/18.
 */
object DetailContract {
    interface View : LoggedView
    interface Presenter : LoggedPresenter<View>
}