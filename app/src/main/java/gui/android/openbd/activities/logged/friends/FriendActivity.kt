package gui.android.openbd.activities.logged.friends

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import gui.android.openbd.R
import gui.android.openbd.activities.logged.LoggedActivity
import gui.android.openbd.activities.logged.friends.search.SearchActivity
import gui.android.openbd.apis.events.EventFriend
import gui.android.openbd.apis.events.EventRemove
import gui.android.openbd.apis.requests.RequestFriend
import gui.android.openbd.tools.PreferenceHelper.FRIEND_COUNT
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import kotterknife.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by jeremy on 25/02/18.
 *
 * Display list of friend with search bar
 */
class FriendActivity:
        LoggedActivity<FriendContract.View, FriendPresenter>(),
        FriendContract.View {
    private val mFriendList: RecyclerView by bindView(R.id.layout_friend_list_view)
    override var mPresenter: FriendPresenter = FriendPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend)
        ButterKnife.bind(this)

        mPresenter.initialize(this)

        drawerNavigationBar(this, resources.getString(R.string.activity_friend_title))

        RequestFriend.instance.setActivity(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        RequestFriend.instance.fetchFriends()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEventSearch(event: EventRemove) {}

    @Subscribe
    fun onEventFriend(event: EventFriend) {
        val prefs = defaultPrefs(this)
        mPresenter.completeListFriend(mFriendList, event, this)
        prefs.edit().putString(FRIEND_COUNT, mFriendList.adapter.itemCount.toString()).apply()
    }

    @OnClick(R.id.activity_friend_search_friend)
    fun onClickSearchFriend() {
        val intent = Intent(this, SearchActivity::class.java)
        startActivity(intent)
    }
}