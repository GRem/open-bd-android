package gui.android.openbd.lists.views.collections

import android.app.Activity
import android.content.Intent
import android.view.View
import gui.android.openbd.activities.logged.details.DetailActivity
import gui.android.openbd.lists.views.ViewHolderBase
import gui.android.openbd.tools.PreferenceHelper.BOOK_ID
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs

/**
 * Created by jeremy on 12/03/18.
 *
 * View adapter for my collection
 */
class ViewHolderMy(view: View, activity: Activity):
        ViewHolderBase(view, activity) {

    init {
        view.setOnClickListener({  _: View ->
            val prefs = defaultPrefs(activity)
            val intent = Intent(view.context, DetailActivity::class.java)

            prefs.edit().putString(BOOK_ID, id.text.toString()).apply()

            activity.startActivity(intent)
        })
    }
}
