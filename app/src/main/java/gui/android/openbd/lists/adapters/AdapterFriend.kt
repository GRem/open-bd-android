package gui.android.openbd.lists.adapters

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import gui.android.openbd.R
import gui.android.openbd.activities.logged.friends.detail.DetailActivity
import gui.android.openbd.apis.models.Friend
import gui.android.openbd.tools.DownloadGravatar
import kotlinx.android.synthetic.main.item_friend.view.*

/**
 * Created by jeremy on 25/02/18.
 *
 * Adapter for friend element
 */
class AdapterFriend(private var mItems: List<Friend>,
                    var activity: Activity):
        RecyclerView.Adapter<AdapterFriend.ViewHolder>() {
    @Suppress("NAME_SHADOWING")
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val id: TextView = view.itemFriendId
        val avatar: ImageView = view.itemFriendAvatar
        val pseudo: TextView = view.itemFriendPseudo
        val email: TextView = view.itemFriendEmail
        val items: TextView = view.itemFriendItems

        init {
            view.setOnClickListener({ view: View ->
                val intent = Intent(view.context, DetailActivity::class.java)

                intent.putExtra("id", id.text)
                        .putExtra("email", email.text)
                        .putExtra("pseudo", pseudo.text)

                activity.startActivity(intent)
            })
        }
    }

    init {
        if (mItems.isEmpty()) {
            val layout: RelativeLayout = activity.findViewById(R.id.collectionEmpty)
            val info: TextView = activity.findViewById(R.id.collectionEmptyText)
            val back: ImageView = activity.findViewById(R.id.collectionEmptyArrow)

            layout.visibility = View.VISIBLE

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                @RequiresApi(Build.VERSION_CODES.N)
                info.text = Html.fromHtml(activity.getString(R.string.activity_friend_list_empty), Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE)
            } else {
                info.text = Html.fromHtml(activity.getString(R.string.activity_friend_list_empty))
            }

            back.setImageResource(R.drawable.ic_arrow_click_button_add)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = LayoutInflater.from(parent.context)
        return ViewHolder(layout.inflate(R.layout.item_friend, parent, false))
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]

        holder.id.text = item.getId()
        holder.pseudo.text = item.getPseudo()
        holder.email.text = item.getEmail()
        holder.items.text = activity.getString(R.string.activity_friend_books_count, item.getItems())
        DownloadGravatar.instance.gravatarImage(item.getEmail(), holder.avatar)
    }
}
