package gui.android.openbd.lists.adapters

import android.app.Activity
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.viethoa.RecyclerViewFastScroller
import gui.android.openbd.R
import gui.android.openbd.apis.models.Collection
import gui.android.openbd.lists.views.ViewHolderBase
import gui.android.openbd.tools.DownloadImage
import gui.android.openbd.tools.constants.API_URL_SIMPLE

/**
 * Created by jeremy on 12/03/18.
 *
 * Adapter for collection
 */
open class AdapterCollectionBase(private var mItems: List<Collection>,
                                 private var mActivity: Activity):
        RecyclerView.Adapter<ViewHolderBase>() {
    init {
        if (mItems.isEmpty()) {
            val layout: RelativeLayout = mActivity.findViewById(R.id.collectionEmpty)
            val info: TextView = mActivity.findViewById(R.id.collectionEmptyText)
            val back: ImageView = mActivity.findViewById(R.id.collectionEmptyArrow)
            val alphabet: RecyclerViewFastScroller = mActivity.findViewById(R.id.layout_collection_list_alphabet)

            alphabet.visibility = View.GONE
            layout.visibility = View.VISIBLE

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                @RequiresApi(Build.VERSION_CODES.N)
                info.text = Html.fromHtml(mActivity.getString(R.string.activity_collection_empty_list), Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE)
            } else {
                @Suppress("DEPRECATION")
                info.text = Html.fromHtml(mActivity.getString(R.string.activity_collection_empty_list))
            }

            back.setImageResource(R.drawable.ic_arrow_click_button_add)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderBase {
        val layout = LayoutInflater.from(parent.context)
        return ViewHolderBase(layout.inflate(R.layout.item_collection, parent, false), mActivity)
    }

    override fun onBindViewHolder(holder: ViewHolderBase, position: Int) {
        val item = mItems[position]

        holder.title.text = getBookTitle(item)
        holder.volume.text = getBookVolume(item)
        getBookCover(item, holder.cover)
        holder.id.text = item.getId()
        holder.isbn.text = item.getIsbn13()
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    fun getItem(position: Int): Collection {
        return mItems[position]
    }

    private fun getBookTitle(item: Collection): String {
        return if (item.getSaga() != item.getTitle()) {
            mActivity.resources.getString(R.string.activity_collection_item, item.getSaga(), item.getTitle())
        } else {
            item.getTitle()
        }
    }

    @Suppress("UselessCallOnNotNull")
    private fun getBookVolume(item: Collection): String {
        return if (item.getVolume().isNullOrEmpty()) {
            " "
        } else {
            mActivity.resources.getString(R.string.activity_collection_volume, item.getVolume())
        }
    }

    private fun getBookCover(item: Collection, cover: ImageView) {
        val dl = DownloadImage(cover)
        return if (item.getCover().isNotEmpty()) {
            cover.setImageBitmap(dl.execute(API_URL_SIMPLE+item.getCover()).get())
        } else {
            cover.setImageResource(R.drawable.ic_if_book_285636)
        }
    }
}