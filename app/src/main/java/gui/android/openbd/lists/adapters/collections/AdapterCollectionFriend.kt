package gui.android.openbd.lists.adapters.collections

import android.app.Activity
import gui.android.openbd.lists.adapters.AdapterCollectionBase
import gui.android.openbd.apis.models.Collection

/**
 * Created by jeremy on 12/03/18.
 *
 * Adapter for collection in view friend detail
 */
class AdapterCollectionFriend(mItems: List<Collection>,
                              activity: Activity):
        AdapterCollectionBase(mItems, activity)
