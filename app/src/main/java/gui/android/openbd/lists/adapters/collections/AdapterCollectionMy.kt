package gui.android.openbd.lists.adapters.collections

import android.app.Activity
import com.viethoa.RecyclerViewFastScroller
import gui.android.openbd.lists.adapters.AdapterCollectionBase
import gui.android.openbd.apis.models.Collection

/**
 * Created by jeremy on 31/12/17.
 *
 * Adapter for collection element in list view
 */
class AdapterCollectionMy(private var mItems: List<Collection>,
                          activity: Activity):
        AdapterCollectionBase(mItems, activity),
        RecyclerViewFastScroller.BubbleTextGetter {

    override fun getTextToShowInBubble(pos: Int): String {
        if (pos < 0 || pos >= mItems.size)
            return null.toString()

        val name = mItems[pos].getSaga()
        return if (name.isEmpty()) null.toString() else mItems[pos].getSaga().substring(0, 1)
    }
}
