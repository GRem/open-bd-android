package gui.android.openbd.lists.views.collections

import android.app.Activity
import android.view.View
import gui.android.openbd.lists.views.ViewHolderBase

/**
 * Created by jeremy on 12/03/18.
 *
 * View adapter for friend collection
 */
class ViewHolderFriend(view: View, activity: Activity):
        ViewHolderBase(view, activity)