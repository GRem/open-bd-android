package gui.android.openbd.lists.views

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import gui.android.openbd.activities.logged.details.DetailActivity
import gui.android.openbd.tools.PreferenceHelper.BOOK_ID
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import kotlinx.android.synthetic.main.item_collection.view.*

/**
 * Created by jeremy on 12/03/18.
 *
 * Base view to adapter collection
 */
open class ViewHolderBase(private var view: View,
                          private var activity: Activity):
        RecyclerView.ViewHolder(view) {
    val id: TextView = view.itemCollectionId
    val title: TextView = view.itemCollectionTitle
    val isbn: TextView = view.itemCollectionIsbn
    val cover: ImageView = view.itemCollectionAvatar
    val volume: TextView = view.itemCollectionVolume

    init {
        view.setOnClickListener({ view: View ->
            val prefs = defaultPrefs(activity)
            val intent = Intent(view.context, DetailActivity::class.java)

            prefs.edit().putString(BOOK_ID, id.text.toString()).apply()

            activity.startActivity(intent)
        })
    }
}