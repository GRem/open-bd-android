package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.apis.models.Friend

/**
 * Created by jeremy on 25/02/18.
 *
 * Response to list friends to current user
 */
class ResponseFriends: General() {
    @Expose
    @SerializedName("friends")
    var friends: List<Friend>? = null
}