package gui.android.openbd.apis.responses

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

/**
 * Created by jeremy on 30/12/17.
 *
 * All response contains this method
 */
open class General {
    @Expose
    @SerializedName("error")
    var error: String? = null
        protected set

    @Expose
    @SerializedName("error_description")
    var errorDescription: String? = null
        protected set

    val isErrorResponse: Boolean
        get() = !TextUtils.isEmpty(error) || !TextUtils.isEmpty(errorDescription)
}
