package gui.android.openbd.apis.models

/**
 * Created by jeremy on 25/02/18.
 *
 * Object represent a friend to user
 */
class Friend(private var id: String,
             private var email: String,
             private var pseudo: String,
             private var items: String) {

    fun getId(): String {
        return id
    }

    fun getEmail(): String {
        return email
    }

    fun getPseudo(): String {
        return pseudo
    }

    fun getItems(): String {
        return items
    }
}
