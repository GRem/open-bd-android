package gui.android.openbd.apis.bodies

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Suppress("unused")
/**
 * Created by jeremy on 23/01/18.
 *
 * Body for register user in service
 */
class BodySignUp(email: String, pseudo: String, password: String, password2: String) {
    @Expose
    @SerializedName("email")
    private val mEmail: String = email

    @Expose
    @SerializedName("pseudo")
    private val mPseudo: String = pseudo

    @Expose
    @SerializedName("password")
    private val mPassword: String = password

    @Expose
    @SerializedName("password2")
    private val mPassword2: String = password2
}