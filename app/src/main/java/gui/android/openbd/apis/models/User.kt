package gui.android.openbd.apis.models

/**
 * Created by jeremy on 05/01/18.
 *
 * Object represent an user connected
 */
class User(access_token: String,
           token_type: String,
           expires_in: Int,
           refresh_token: String) {
    private var mAccessToken: String = access_token
    private var mTokenType: String = token_type
    private var mExpireIn: Int = expires_in
    private var mRefreshToken: String = refresh_token

    fun getToken(): String {
        return mAccessToken
    }
}
