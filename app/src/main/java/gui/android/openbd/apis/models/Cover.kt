package gui.android.openbd.apis.models

import android.util.Log

/**
 * Created by jeremy on 14/01/18.
 *
 * Model to Cover resource
 */
class Cover(private var first: Url) {
   class Url(private var url: String,
             private var thumb: ThumbUrl,
             private var small: SmallUrl) {
      class ThumbUrl(private var url: String) {
        fun getUrl(): String {
            val tmp: ArrayList<String> = url.split("/") as ArrayList<String>
            var newUrl = ""

            tmp[tmp.lastIndex] = "?filter=thumb"

            for (s in tmp) {
                Log.d("URL", s)
                newUrl += s
            }

           return newUrl
        }
      }
      class SmallUrl(private var url: String) {
         fun getUrl(): String {
             val tmp: ArrayList<String> = url.split("/") as ArrayList<String>
             var newUrl = ""

             tmp[tmp.lastIndex] = "?filter=small"

             for (s in tmp) {
                 Log.d("URL", s)
                 newUrl += s
             }
            return newUrl
         }
      }

       fun getNormalUrl(): String {
           val tmp: ArrayList<String> = url.split("/") as ArrayList<String>
           var newUrl = ""

           tmp[tmp.lastIndex] = "?filter=normal"

           for (s in tmp) {
               newUrl += "/$s"
           }

           return newUrl
       }

       fun getThumbUrl(): String {
           return thumb.getUrl()
       }

       fun getSmallUrl(): String {
          return small.getUrl()
       }
   }

   fun getUrlNormal(): String {
      return first.getNormalUrl()
   }

    fun getUrlThumb(): String {
        return first.getThumbUrl()
    }

    fun getUrlSmall(): String {
        return first.getSmallUrl()
    }
}
