package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jeremy on 14/04/18.
 *
 * Manage object JSON returning by API for stat resource
 */
class ResponseStat: General() {
    @Expose
    @SerializedName("book_count")
    var bookCount: Int? = null

    @Expose
    @SerializedName("book_prices")
    var bookPrices: Float? = null
}