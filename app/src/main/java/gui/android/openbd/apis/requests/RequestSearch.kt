package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import android.util.Log
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.events.EventFriends
import gui.android.openbd.apis.events.EventSearch
import gui.android.openbd.apis.models.Friend
import gui.android.openbd.apis.models.Search
import gui.android.openbd.apis.responses.ResponseSearch
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 16/01/18.
 *
 * Manage request for search in service Open BD
 */
class RequestSearch: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestSearch()
    }

    companion object {
        val instance: RequestSearch by lazy { Holder.INSTANCE }
    }

    fun searchISBN13(isbn: String) {
        Log.d(TAG, "fetch Collection ...")
        val callResponse = api.searchISBN(isbn)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseSearch> {
            override fun onSuccess(call: Call<ResponseSearch>, response: Response<ResponseSearch>) {
                debugResponse(response)
                val search: Search = response.body()!!.getResult()
                EventBus.getDefault().post(EventSearch(true, search))
            }

            override fun onError(call: Call<ResponseSearch>, response: Response<ResponseSearch>) {
                error()
                EventBus.getDefault().post(Event(false))
            }

            override fun onNotFound(call: Call<ResponseSearch>, response: Response<ResponseSearch>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseSearch>, response: Response<ResponseSearch>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseSearch>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseSearch>, response: Response<ResponseSearch>) {
                tokenExpired()
            }
        }))
    }

    fun searchFriends(user: String) {
        Log.d(TAG, "Search a friend")
        val callResponse = api.searchFriends(user)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<Array<Friend>> {
            override fun onSuccess(call: Call<Array<Friend>>, response: Response<Array<Friend>>) {
                val friends: Array<Friend>? = response.body()
                debugResponse(response)
                EventBus.getDefault().post(EventFriends(true, friends))
            }

            override fun onError(call: Call<Array<Friend>>, response: Response<Array<Friend>>) {
                EventBus.getDefault().post(EventFriends(false, null))
                error()
            }

            override fun onNotFound(call: Call<Array<Friend>>, response: Response<Array<Friend>>) {
                EventBus.getDefault().post(EventFriends(false, null))
                notFound()
            }

            override fun onTokenInvalid(call: Call<Array<Friend>>, response: Response<Array<Friend>>) {
                EventBus.getDefault().post(EventFriends(false, null))
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<Array<Friend>>, t: Throwable) {
                EventBus.getDefault().post(EventFriends(false, null))
                networkError()
            }

            override fun onTokenExpired(call: Call<Array<Friend>>, response: Response<Array<Friend>>) {
                tokenExpired()
            }
        }))
    }
}
