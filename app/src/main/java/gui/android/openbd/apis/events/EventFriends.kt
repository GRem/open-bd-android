package gui.android.openbd.apis.events

import gui.android.openbd.apis.models.Friend

/**
 * Created by jeremy on 11/03/18.
 *
 * Event for friend resource
 */
class EventFriends(isSuccess: Boolean,
                  val list: Array<Friend>?): Event(isSuccess) {
    fun getListFriend(): Array<Friend>? {
        return list
    }
}

