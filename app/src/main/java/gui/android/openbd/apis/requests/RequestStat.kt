package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import android.util.Log
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.EventStat
import gui.android.openbd.apis.responses.ResponseStat
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 14/04/18.
 *
 * Manage request to stat resource
 */
class RequestStat: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestStat()
    }

    companion object {
        val instance: RequestStat by lazy { Holder.INSTANCE }
    }

    fun fetchStatUser() {
        Log.d(TAG, "Fetch stat user")
        val callResponse = api.fetchStatUser()

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseStat> {
            override fun onSuccess(call: Call<ResponseStat>, response: Response<ResponseStat>) {
                debugResponse(response.body()!!)
                EventBus.getDefault().post(EventStat(true, response.body()!!.bookCount!!, response.body()!!.bookPrices!!))
            }

            override fun onError(call: Call<ResponseStat>, response: Response<ResponseStat>) {
                error()
            }

            override fun onNotFound(call: Call<ResponseStat>, response: Response<ResponseStat>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseStat>, response: Response<ResponseStat>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseStat>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseStat>, response: Response<ResponseStat>) {
                tokenExpired()
            }
        }))
    }
}
