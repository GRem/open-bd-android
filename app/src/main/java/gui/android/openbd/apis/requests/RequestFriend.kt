package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import android.util.Log
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.events.EventCollection
import gui.android.openbd.apis.events.EventFriend
import gui.android.openbd.apis.models.Collection
import gui.android.openbd.apis.models.Friend
import gui.android.openbd.apis.responses.ResponseCollections
import gui.android.openbd.apis.responses.ResponseFriend
import gui.android.openbd.apis.responses.ResponseFriends
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 25/02/18.
 *
 * Manage request to resource Friend
 */
class RequestFriend: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestFriend()
    }

    companion object {
        val instance: RequestFriend by lazy { Holder.INSTANCE }
    }

    fun fetchFriends() {
        Log.d(TAG, "List friends")
        val callResponse = api.fetchFriends()

        callResponse.enqueue(Asynchronus(object: IAsynchronus<ResponseFriends> {
            override fun onSuccess(call: Call<ResponseFriends>, response: Response<ResponseFriends>) {
                val friends: List<Friend> = response.body()!!.friends!!
                EventBus.getDefault().post(EventFriend(true, friends))
            }

            override fun onError(call: Call<ResponseFriends>, response: Response<ResponseFriends>) {
                debugResponse(response)
                error()
            }

            override fun onNotFound(call: Call<ResponseFriends>, response: Response<ResponseFriends>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseFriends>, response: Response<ResponseFriends>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseFriends>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseFriends>, response: Response<ResponseFriends>) {
                tokenExpired()
            }
        }))
    }

    fun fetchCollectionToUser(userId: String) {
        Log.d(TAG, "List collection to friend")
        val callResponse = api.fetchCollectionToUser(userId)

        callResponse.enqueue(Asynchronus(object: IAsynchronus<ResponseCollections> {
            override fun onSuccess(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                val collections: List<Collection> = response.body()!!.collections!!
                EventBus.getDefault().post(EventCollection(true, collections))
            }

            override fun onError(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                error()
            }

            override fun onNotFound(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseCollections>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                tokenExpired()
            }
        }))
    }

    fun addNewFriend(userId: String) {
        Log.d(TAG, "Add friend")
        val callResponse = api.addNewFriend(userId)

        callResponse.enqueue(Asynchronus(object: IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(Event(true))
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                error()
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }

    fun removeFriend(userId: String) {
        Log.d(TAG, "Add friend")
        val callResponse = api.removeFriend(userId)

        callResponse.enqueue(Asynchronus(object: IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(Event(true))
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                error()
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }

    fun showFriend(userId: String) {
        Log.d(TAG, "Add friend")
        val callResponse = api.showFriend(userId)

        callResponse.enqueue(Asynchronus(object: IAsynchronus<ResponseFriend> {
            override fun onSuccess(call: Call<ResponseFriend>, response: Response<ResponseFriend>) {
                EventBus.getDefault().post(Event(true))
            }

            override fun onError(call: Call<ResponseFriend>, response: Response<ResponseFriend>) {
                error()
            }

            override fun onNotFound(call: Call<ResponseFriend>, response: Response<ResponseFriend>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseFriend>, response: Response<ResponseFriend>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseFriend>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseFriend>, response: Response<ResponseFriend>) {
                tokenExpired()
            }
        }))
    }
}
