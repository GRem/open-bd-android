package gui.android.openbd.apis.events

import gui.android.openbd.apis.models.Collection

/**
 * Created by jeremy on 31/12/17.
 *
 * Event for collection resource
 */
class EventCollection(isSuccess: Boolean,
                      val list: List<Collection>): Event(isSuccess)
