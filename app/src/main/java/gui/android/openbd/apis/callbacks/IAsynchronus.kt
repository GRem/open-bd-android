package gui.android.openbd.apis.callbacks

import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 30/12/17.
 *
 * Interface for manager to requester response
 */
interface IAsynchronus<T> {
    fun onSuccess(call: Call<T>, response: Response<T>)
    fun onError(call: Call<T>, response: Response<T>)
    fun onNotFound(call: Call<T>, response: Response<T>)
    fun onTokenInvalid(call: Call<T>, response: Response<T>)
    fun onNetWorkError(call: Call<T>, t: Throwable)
    fun onTokenExpired(call: Call<T>, response: Response<T>)
}
