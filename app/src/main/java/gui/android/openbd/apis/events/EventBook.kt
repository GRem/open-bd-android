package gui.android.openbd.apis.events

import gui.android.openbd.apis.models.Book

/**
 * Created by jeremy on 13/01/18.
 *
 * Event for book resource
 */
class EventBook(isSuccess: Boolean, val book: Book): Event(isSuccess)
