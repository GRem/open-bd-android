package gui.android.openbd.apis.events

import gui.android.openbd.apis.responses.ResponseSignIn

/**
 * Created by jeremy on 08/01/18.
 *
 * Event for Sign In
 */
class EventSignIn(isSuccess: Boolean, private val authResponse: ResponseSignIn?): Event(isSuccess) {

    constructor(isSuccess: Boolean) : this(isSuccess, null)

    fun readToken(): String {
        return authResponse!!.getAccessToken()
    }

    fun readRefreshToken(): String {
        return authResponse!!.getRefreshToken()
    }

    fun readPseudo(): String {
        return authResponse!!.getPseudo()
    }
}
