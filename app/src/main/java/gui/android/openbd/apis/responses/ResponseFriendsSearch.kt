package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import gui.android.openbd.apis.models.Friend

/**
 * Created by jeremy on 05/03/18.
 *
 * List element to friend searched
 */
class ResponseFriendsSearch: General() {
    @Expose
    var item : Array<Friend>? = null
}
