package gui.android.openbd.apis.requests

import android.app.Activity
import android.util.Log
import gui.android.openbd.R
import com.google.gson.GsonBuilder

/**
 * Created by jeremy on 29/12/17.
 *
 * Manage request to collection resource
 */
abstract class Request {
    @Suppress("PropertyName")
    protected val TAG: String = this.javaClass.simpleName
    private lateinit var mActivity: Activity

    protected fun getActivity(): Activity {
        return mActivity
    }

    fun setActivity(mActivity: Activity) {
        this.mActivity = mActivity
    }

    protected fun error() {
        Log.d(TAG, getActivity().resources.getString(R.string.api_request_error, getActivity().resources.getString(R.string.api_message_error)))
    }

    protected fun notFound() {
        Log.d(TAG, getActivity().resources.getString(R.string.api_request_error, getActivity().resources.getString(R.string.api_message_not_found)))
    }

    protected fun tokenInvalid() {
        Log.d(TAG, getActivity().resources.getString(R.string.api_request_error, getActivity().resources.getString(R.string.api_message_token_invalid)))
    }

    protected fun networkError() {
        Log.d(TAG, getActivity().resources.getString(R.string.api_request_error, getActivity().resources.getString(R.string.api_message_network_error)))
    }

    protected fun tokenExpired() {
        Log.d(TAG, getActivity().resources.getString(R.string.api_request_error, getActivity().resources.getString(R.string.api_message_token_expired)))
    }

    protected fun debugResponse(responseBody: Any) {
        Log.d(TAG, "Read response ...")
        val gson = GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create()
        Log.d(TAG, gson.toJson(responseBody))
    }
}