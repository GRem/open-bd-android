package gui.android.openbd.apis.models

import java.io.Serializable

/**
 * Created by jeremy on 16/01/18.
 *
 * Model for Search resource
 */
data class Search(private var bookId: String,
                  private var title: String,
                  private var year: String,
                  private var isbn13: String) : Serializable {
    fun getBookId(): String {
        return bookId
    }

    fun getTitle(): String {
        return title
    }

    fun getYear(): String {
        return year
    }

    fun getIsbn13(): String {
        return isbn13
    }
}