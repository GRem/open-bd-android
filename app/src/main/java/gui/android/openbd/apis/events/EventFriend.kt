package gui.android.openbd.apis.events

import gui.android.openbd.apis.models.Friend

/**
 * Created by jeremy on 25/02/18.
 *
 * Event for friend resource
 */
class EventFriend(isSuccess: Boolean,
                  val list: List<Friend>?): Event(isSuccess) {
    fun getListFriend(): List<Friend>? {
        return list
    }
}
