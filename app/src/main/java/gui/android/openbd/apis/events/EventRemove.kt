package gui.android.openbd.apis.events

/**
 * Created by jeremy on 03/02/18.
 *
 * Event when use delete book to collection
 */
class EventRemove(override var isSuccess: Boolean): Event(isSuccess)