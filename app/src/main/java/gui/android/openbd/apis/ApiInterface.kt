package gui.android.openbd.apis

import gui.android.openbd.apis.bodies.BodySignIn
import gui.android.openbd.apis.bodies.BodySignInRefresh
import gui.android.openbd.apis.bodies.BodySignUp
import gui.android.openbd.apis.models.Friend
import gui.android.openbd.apis.responses.*
import gui.android.openbd.tools.constants.API_URL
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by jeremy on 27/12/17.
 *
 * List all routes used in application
 */
interface ApiInterface {
    // region ===Sign===
    @POST(API_URL+"sign/token")
    fun singIn(@Body body: BodySignIn): Call<ResponseSignIn>

    @POST(API_URL+"sign/token")
    fun signRefresh(@Body body: BodySignInRefresh): Call<ResponseSignIn>

    @POST(API_URL+"sign/revoke")
    fun signDown(@Query("client_id") clientId: String,
                 @Query("client_secret") clientSecret: String): Call<Void>

    @POST(API_URL+"sign/up")
    fun signUp(@Body body: BodySignUp): Call<Void>
    // endregion

    // region ===Collection===
    @GET(API_URL+"collections")
    fun fetchCollections(): Call<ResponseCollections>

    @POST(API_URL+"collections/items/{book_id}")
    fun addBookToCollection(@Path("book_id") bookId: String): Call<Void>

    @DELETE(API_URL+"collections/items/{book_id}")
    fun removeBookToCollection(@Path("book_id") bookUd: String): Call<Void>
    // endregion

    // region ===Book===
    @GET(API_URL+"books/{book_id}")
    fun showBook(@Path("book_id") bookId: String): Call<ResponseBook>
    // endregion

    // region ===Search===
    @GET(API_URL+"search/books/{isbn}")
    fun searchISBN(@Path("isbn") isbn: String): Call<ResponseSearch>

    @GET(API_URL+"search/friends")
    fun searchFriends(@Query("user") user: String): Call<Array<Friend>>
    //endregion

    // region ===Friend===
    @GET(API_URL+"friends")
    fun fetchFriends(): Call<ResponseFriends>

    @GET(API_URL+"friends/{user_id}/collections")
    fun fetchCollectionToUser(@Path("user_id") userId: String): Call<ResponseCollections>

    @DELETE(API_URL+"friends/{user_id}")
    fun removeFriend(@Path("user_id") userId: String): Call<Void>

    @POST(API_URL+"friends/{user_id}")
    fun addNewFriend(@Path("user_id") userId: String): Call<Void>

    @GET(API_URL+"friends/{user_id}")
    fun showFriend(@Path("user_id") userId: String): Call<ResponseFriend>
    // endregion

    // region ===Stats===
    @GET(API_URL+"stats")
    fun fetchStatUser(): Call<ResponseStat>
    // endregion
}