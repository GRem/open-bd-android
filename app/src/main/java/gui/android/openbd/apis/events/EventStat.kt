package gui.android.openbd.apis.events

/**
 * Created by jeremy on 14/04/18.
 *
 * Manage event about stat resource
 */
class EventStat(isSuccess: Boolean,
                private var number_book: Int,
                private var price_book: Float) : Event(isSuccess) {
    fun getBookNumber(): Int {
        return number_book
    }

    fun getBookPrice(): Float {
        return price_book
    }
}