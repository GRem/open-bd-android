package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.apis.models.Search

/**
 * Created by jeremy on 16/01/18.
 *
 * Object represent response for request Search
 */
class ResponseSearch: General() {
    @Expose
    @SerializedName("id")
    private var bookId: String = ""

    @Expose
    @SerializedName("title")
    private var title: String = ""

    @Expose
    @SerializedName("year")
    private var year: String = ""

    @Expose
    @SerializedName("isbn13")
    private var isbn13: String = ""

    fun getResult(): Search {
        return Search(bookId, title, year, isbn13)
    }
}