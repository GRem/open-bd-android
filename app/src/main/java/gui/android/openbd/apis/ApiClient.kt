package gui.android.openbd.apis

import retrofit2.Retrofit
import com.google.gson.GsonBuilder
import gui.android.openbd.OpenBD
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import gui.android.openbd.tools.constants.API_TIMEOUT_SEC
import gui.android.openbd.tools.constants.API_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by jeremy on 27/12/17.
 *
 * Object for manipulating connection with API
 */
class ApiClient {
    private object Holder {
        val INSTANCE = ApiClient()
    }

    companion object {
        val instance: ApiClient by lazy { Holder.INSTANCE }
    }

    private val headerInterceptor = Interceptor { chain ->
        val original = chain.request()
        val request = original.newBuilder()
                .addHeader("Accept-Language", Locale.getDefault().language)

        val prefs = defaultPrefs(OpenBD.getOpenBDContext()!!)
        val token: String = prefs.getString("token", "")
        if (token.isNotEmpty()) {
            request.addHeader("Authorization", "Bearer $token")
        }

        chain.proceed(request.build())
    }

    fun getApiClient(): ApiInterface {
        // Add an interceptor for log level
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS

        // Build client request
        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(headerInterceptor)
                .connectTimeout(API_TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(API_TIMEOUT_SEC, TimeUnit.SECONDS)
                .build()

        // Build JSon object associate to request
        val gson = GsonBuilder()
                .setLenient()
                .serializeNulls()
                .create()

        // Build request
        val retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()

        return retrofit.create(ApiInterface::class.java)
    }
}