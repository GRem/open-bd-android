package gui.android.openbd.apis.models

import gui.android.openbd.tools.ISBN

/**
 * Created by jeremy on 13/01/18.
 *
 * Object represent a book
 */
class Book(private var id: String,
           private var name: String?,
           private var saga: String?,
           private var isbn13: String?,
           private var isbn10: String?,
           private var description: String?,
           private var categories: Array<String>?,
           private var authors: Array<String>?,
           private var cover: Cover?) {

    fun getId(): String {
        return id
    }

    fun getName(): String? {
        return name
    }

    fun getSaga(): String? {
        return saga
    }

    fun hasSaga(): Boolean {
        return !saga.isNullOrEmpty()
    }

    fun hasIsbn10(): Boolean {
        return !isbn10.isNullOrEmpty()
    }

    fun getIsbn10(): String {
        return ISBN.transformIsbn10(isbn10!!)
    }

    fun hasIsbn13(): Boolean {
        return !isbn13.isNullOrEmpty()
    }

    fun getIsbn13(): String {
        return ISBN.transformIsbn13(isbn13!!)
    }

    fun hasCategories(): Boolean {
        return categories != null
    }

    fun getCategories(): Array<String> {
        return categories!!
    }

    fun hasAuthors(): Boolean {
        return authors != null
    }

    fun getAuthors(): Array<String> {
        return authors!!
    }

    fun getDescription(): String {
        return description!!
    }

    fun hasCover(): Boolean {
        return cover != null
    }

    fun getCoverNormal(): String {
        return cover!!.getUrlNormal()
    }

    fun getCoverThumb(): String {
        return cover!!.getUrlThumb()
    }

    fun getCoverSmall(): String {
        return cover!!.getUrlSmall()
    }
}
