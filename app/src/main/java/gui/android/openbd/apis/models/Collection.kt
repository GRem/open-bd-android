package gui.android.openbd.apis.models

/**
 * Created by jeremy on 30/12/17.
 *
 * Object represent an collection to user connected
 */
class Collection(private var id: String,
                 private var saga: String,
                 private var isbn13: String,
                 private var title: String,
                 private var volume: String,
                 private var cover: Cover) {
    fun getId(): String {
        return id
    }

    fun getSaga(): String {
        return saga
    }

    fun getTitle(): String {
        return title
    }

    fun getVolume(): String {
        return volume
    }

    @Suppress("SENSELESS_COMPARISON")
    fun getCover(): String {
        return if (cover != null) {
            cover.getUrlNormal()
        } else {
            ""
        }
    }

    fun getIsbn13(): String {
        var isbn: String = isbn13.substring(0, 3) + "-"

        isbn += isbn13.substring(3, 4) + "-"
        isbn += isbn13.substring(4, 8) + "-"
        isbn += isbn13.substring(8, 12) + "-"
        isbn += isbn13.substring(12, 13)

        return isbn
    }
}