package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.apis.models.Collection

/**
 * Created by jeremy on 30/12/17.
 *
 * Manage object JSON returning by API for collection resource
 */
class ResponseCollections: General() {
    @Expose
    @SerializedName("books")
    var collections: List<Collection>? = null
}