package gui.android.openbd.apis.events

import gui.android.openbd.apis.models.Search

/**
 * Created by jeremy on 16/01/18.
 *
 * Event when search request response is received
 */
class EventSearch(override var isSuccess: Boolean,
                  private var resultat: Search) : Event(isSuccess) {

    fun getResultat(): Search {
        return resultat
    }
}
