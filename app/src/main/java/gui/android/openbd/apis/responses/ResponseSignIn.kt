package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jeremy on 05/01/18.
 *
 * Manage authentication user in service
 */
class ResponseSignIn(@Expose
                     @SerializedName("refresh_token")
                     private var mRefreshToken: String, @Expose
                     @SerializedName("access_token")
                     private var mAccessToken: String, @Expose
                     @SerializedName("token_type")
                     private var mTokenType: String, @Expose
                     @SerializedName("expires_in")
                     private var mExpireIn: Int, @Expose
                     @SerializedName("pseudo")
                     private var mPseudonym: String) : General() {
    fun getRefreshToken(): String {
        return mRefreshToken
    }

    fun getAccessToken(): String {
        return mAccessToken
    }

    fun getTokenType(): String {
        return mTokenType
    }

    fun getExpire(): Int {
        return mExpireIn
    }

    fun getPseudo(): String {
        return mPseudonym
    }
}
