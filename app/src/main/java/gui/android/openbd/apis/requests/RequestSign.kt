package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.bodies.BodySignIn
import gui.android.openbd.apis.bodies.BodySignInRefresh
import gui.android.openbd.apis.bodies.BodySignUp
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.Event
import gui.android.openbd.apis.events.EventSignIn
import gui.android.openbd.apis.responses.ResponseSignIn
import gui.android.openbd.tools.PreferenceHelper.TOKEN_REFRESH
import gui.android.openbd.tools.PreferenceHelper.defaultPrefs
import gui.android.openbd.tools.constants.API_CLIENT_ID
import gui.android.openbd.tools.constants.API_CLIENT_SECRET
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 05/01/18.
 *
 * Manage request to collection resource
 */
class RequestSign : Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestSign()
    }

    companion object {
        val instance: RequestSign by lazy { Holder.INSTANCE }
    }

    fun singIn(email: String, password: String) {
        val callResponse = api.singIn(BodySignIn(email, password))

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseSignIn> {
            override fun onSuccess(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                val auth: ResponseSignIn = response.body()!!

                EventBus.getDefault().post(EventSignIn(true, auth))
            }

            override fun onError(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                error()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onNotFound(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                notFound()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onTokenInvalid(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                val prefs = defaultPrefs(getActivity())
                signRefresh(prefs.getString(TOKEN_REFRESH, ""))
            }

            override fun onNetWorkError(call: Call<ResponseSignIn>, t: Throwable) {
                networkError()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onTokenExpired(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                tokenExpired()
            }
        }))
    }

    fun signDown() {
        val callResponse = api.signDown(API_CLIENT_ID, API_CLIENT_SECRET)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(Event(true))
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                error()
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                tokenInvalid()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }

    fun signRefresh(refreshToken: String) {
        val callResponse = api.signRefresh(BodySignInRefresh(refreshToken))

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseSignIn> {
            override fun onSuccess(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                val auth: ResponseSignIn = response.body()!!

                EventBus.getDefault().post(EventSignIn(true, auth))
            }

            override fun onError(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                error()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onNotFound(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                notFound()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onTokenInvalid(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                tokenInvalid()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onNetWorkError(call: Call<ResponseSignIn>, t: Throwable) {
                networkError()
                EventBus.getDefault().post(EventSignIn(false))
            }

            override fun onTokenExpired(call: Call<ResponseSignIn>, response: Response<ResponseSignIn>) {
                tokenExpired()
            }
        }))
    }

    fun signUp(email: String, pseudo: String, password: String, password2: String) {
        val callResponse = api.signUp(BodySignUp(email, pseudo, password, password2))

        callResponse.enqueue(Asynchronus(object : IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(Event(true))
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                error()
                EventBus.getDefault().post(Event(false))
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                notFound()
                EventBus.getDefault().post(Event(false))
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                tokenInvalid()
                EventBus.getDefault().post(Event(false))
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                networkError()
                EventBus.getDefault().post(Event(false))
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }
}
