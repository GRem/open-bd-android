package gui.android.openbd.apis.models

/**
 * Created by jeremy on 16/01/18.
 *
 * Object represent a author resource
 */
class Author(private var name: String) {
    fun getName(): String {
        return name
    }
}
