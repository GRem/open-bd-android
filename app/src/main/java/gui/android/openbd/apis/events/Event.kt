package gui.android.openbd.apis.events

/**
 * Created by jeremy on 08/01/18.
 *
 * Primary class for all event to this app
 */
open class Event(open val isSuccess: Boolean) {
    companion object
}
