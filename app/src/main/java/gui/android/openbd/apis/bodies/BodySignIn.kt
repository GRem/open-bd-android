@file:Suppress("unused")

package gui.android.openbd.apis.bodies

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.tools.constants.API_CLIENT_ID
import gui.android.openbd.tools.constants.API_CLIENT_SECRET
import gui.android.openbd.tools.constants.API_GRANT_TYPE_PASSWORD

/**
 * Created by jeremy on 07/01/18.
 *
 * Body for sign to service
 */
class BodySignIn(email: String, password: String) {
    @Expose
    @SerializedName("email")
    private val mEmail: String = email

    @Expose
    @SerializedName("password")
    private val mPassword: String = password

    @Expose
    @SerializedName("grant_type")
    private val mGrantType: String = API_GRANT_TYPE_PASSWORD

    @Expose
    @SerializedName("client_id")
    private val mClientId: String = API_CLIENT_ID

    @Expose
    @SerializedName("client_secret")
    private val mClientSecret: String = API_CLIENT_SECRET
}
