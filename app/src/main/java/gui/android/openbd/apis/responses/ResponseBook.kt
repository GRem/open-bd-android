package gui.android.openbd.apis.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.apis.models.Book
import gui.android.openbd.apis.models.Cover

/**
 * Created by jeremy on 13/01/18.
 *
 * Manage object JSON returning by API for book resource
 */
class ResponseBook: General() {
    @Expose
    @SerializedName("id")
    lateinit var id: String

    @Expose
    @SerializedName("title")
    private var title: String? = ""

    @Expose
    @SerializedName("saga")
    private var saga: String? = ""

    @Expose
    @SerializedName("isbn13")
    private var isbn13: String? = ""

    @SerializedName("isbn10")
    private var isbn10: String? = ""

    @Expose
    @SerializedName("categories")
    private var categories: Array<String>? = null

    @Expose
    @SerializedName("authors")
    private var authors: Array<String>? = null

    @Expose
    @SerializedName("description")
    private var description: String? = ""

    @Expose
    @SerializedName("cover")
    private var covers: Cover? = null

    fun book(): Book {
        return Book(
                id,
                title,
                saga,
                isbn13,
                isbn10,
                description,
                categories,
                authors,
                covers
        )
    }
}