package gui.android.openbd.apis.bodies

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import gui.android.openbd.tools.constants.API_CLIENT_ID
import gui.android.openbd.tools.constants.API_CLIENT_SECRET
import gui.android.openbd.tools.constants.API_GRANT_TYPE_REFRESH

@Suppress("unused")
/**
 * Created by jeremy on 23/01/18.
 *
 * Body for refresh token to service
 */
class BodySignInRefresh(refreshToken: String) {
    @Expose
    @SerializedName("client_id")
    private val mClientId: String = API_CLIENT_ID

    @Expose
    @SerializedName("client_secret")
    private val mClientSecret: String = API_CLIENT_SECRET

    @Expose
    @SerializedName("grant_type")
    private val mGrantType: String = API_GRANT_TYPE_REFRESH

    @Expose
    @SerializedName("refresh_token")
    private var mRefreshToken: String = refreshToken
}
