package gui.android.openbd.apis.models

/**
 * Created by jeremy on 13/01/18.
 *
 * Object represent a category resource
 */
class Category(private var name: String) {
    fun getName(): String {
        return name
    }
}