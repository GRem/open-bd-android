package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import gui.android.openbd.apis.ApiClient
import retrofit2.Call
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.EventCollection
import gui.android.openbd.apis.models.Collection
import gui.android.openbd.apis.responses.ResponseCollections
import org.greenrobot.eventbus.EventBus
import retrofit2.Response

/**
 * Created by jeremy on 29/12/17.
 *
 * Manage request to collection resource
 */
class RequestCollection: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestCollection()
    }

    companion object {
        val instance: RequestCollection by lazy { Holder.INSTANCE }
    }

    fun fetchCollections() {
        val callResponse = api.fetchCollections()

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseCollections> {
            override fun onSuccess(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                val collections: List<Collection> = response.body()!!.collections!!
                EventBus.getDefault().post(EventCollection(true, collections))
            }

            override fun onError(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                error()
            }

            override fun onNotFound(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                notFound()
            }
            override fun onTokenInvalid(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<ResponseCollections>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<ResponseCollections>, response: Response<ResponseCollections>) {
                tokenExpired()
            }
        }))
    }
}