package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import android.util.Log
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.EventBook
import gui.android.openbd.apis.models.Book
import gui.android.openbd.apis.responses.ResponseBook
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 13/01/18.
 *
 * Manage request to book resource
 */
class RequestBook: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestBook()
    }

    companion object {
        val instance: RequestBook by lazy { Holder.INSTANCE }
    }

    fun showBook(bookId: String) {
        Log.d(TAG, "show book with id --> $bookId")
        val callResponse = api.showBook(bookId)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<ResponseBook> {
            override fun onSuccess(call: Call<ResponseBook>, response: Response<ResponseBook>) {
                debugResponse(response)
                val book: Book = response.body()!!.book()
                Log.d(TAG, "Book -> "+book.getName())
                EventBus.getDefault().post(EventBook(true, book))
            }

            override fun onNetWorkError(call: Call<ResponseBook>, t: Throwable) {
                networkError()
            }

            override fun onError(call: Call<ResponseBook>, response: Response<ResponseBook>) {
                error()
            }

            override fun onNotFound(call: Call<ResponseBook>, response: Response<ResponseBook>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<ResponseBook>, response: Response<ResponseBook>) {
                tokenInvalid()
            }

            override fun onTokenExpired(call: Call<ResponseBook>, response: Response<ResponseBook>) {
                tokenExpired()
            }
        }))
    }
}
