package gui.android.openbd.apis.requests

import android.annotation.SuppressLint
import android.util.Log
import gui.android.openbd.apis.ApiClient
import gui.android.openbd.apis.ApiInterface
import gui.android.openbd.apis.callbacks.Asynchronus
import gui.android.openbd.apis.callbacks.IAsynchronus
import gui.android.openbd.apis.events.EventRemove
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Response

/**
 * Created by jeremy on 17/01/18.
 *
 * Manage request for user in service Open BD
 */
class RequestUser: Request() {
    private val api: ApiInterface = ApiClient.instance.getApiClient()

    private object Holder {
        @SuppressLint("StaticFieldLeak")
        val INSTANCE = RequestUser()
    }

    companion object {
        val instance: RequestUser by lazy { Holder.INSTANCE }
    }

    fun addBokToCollection(bookId: String) {
        Log.d(TAG, "add book to user collection ...")
        val callResponse = api.addBookToCollection(bookId)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                debugResponse(response)
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                error()
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                notFound()
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                networkError()
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }

    fun removeBookToCollection(bookId: String) {
        Log.d(TAG, "remove book to user collection ...")
        val callResponse = api.removeBookToCollection(bookId)

        callResponse.enqueue(Asynchronus(object : IAsynchronus<Void> {
            override fun onSuccess(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(EventRemove(true))
            }

            override fun onError(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(EventRemove(false))
                error()
            }

            override fun onNotFound(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(EventRemove(false))
                notFound()
            }

            override fun onTokenInvalid(call: Call<Void>, response: Response<Void>) {
                EventBus.getDefault().post(EventRemove(false))
                tokenInvalid()
            }

            override fun onNetWorkError(call: Call<Void>, t: Throwable) {
                EventBus.getDefault().post(EventRemove(false))
                networkError()
            }

            override fun onTokenExpired(call: Call<Void>, response: Response<Void>) {
                tokenExpired()
            }
        }))
    }
}
