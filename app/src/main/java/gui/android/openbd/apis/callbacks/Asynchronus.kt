package gui.android.openbd.apis.callbacks

import gui.android.openbd.tools.constants.HTTP_CODE_401
import gui.android.openbd.tools.constants.HTTP_CODE_403
import gui.android.openbd.tools.constants.HTTP_CODE_404
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jeremy on 30/12/17.
 *
 * Manage response requester
 */
class Asynchronus<T>(private val callBack: IAsynchronus<T>) : Callback<T> {
    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            callBack.onSuccess(call, response)
        } else {
            when (response.code()) {
                HTTP_CODE_401 -> callBack.onTokenInvalid(call, response)
                HTTP_CODE_403 -> callBack.onError(call, response)
                HTTP_CODE_404 -> callBack.onNotFound(call, response)
                else -> callBack.onError(call, response)
            }
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        callBack.onNetWorkError(call, t)
    }
}
