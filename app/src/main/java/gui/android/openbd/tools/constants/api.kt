package gui.android.openbd.tools.constants

import gui.android.openbd.BuildConfig

/**
 * Created by jeremy on 29/12/17.
 *
 * Constants for API connection
 */
private const val API_SHEME: String =   BuildConfig.PROTOCOL
private const val API_URI: String =     BuildConfig.ENDPOINT
private const val API_PREFIX: String =  "/"
private const val API_VERSION: String = "v1/"

const val API_GRANT_TYPE_PASSWORD =     "password"
const val API_GRANT_TYPE_REFRESH =      "refresh_token"
const val API_CLIENT_ID: String =       BuildConfig.CLIENT_ID
const val API_CLIENT_SECRET: String =   BuildConfig.CLIENT_SECRET

const val API_URL_SIMPLE: String =      "$API_SHEME://$API_URI"
const val API_URL: String =             "$API_URL_SIMPLE$API_PREFIX$API_VERSION"
const val API_TIMEOUT_SEC: Long =       30

const val HTTP_CODE_401: Int =          401
const val HTTP_CODE_403: Int =          403
const val HTTP_CODE_404: Int =          404