package gui.android.openbd.tools

import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Created by jeremy on 11/03/18.
 *
 * Download gravatar to user
 */
class DownloadGravatar {
    private object Holder {
        val INSTANCE = DownloadGravatar()
    }

    companion object {
        val instance: DownloadGravatar by lazy { Holder.INSTANCE }
    }

    fun gravatarImage(emailUser: String, avatar: ImageView) {
        Picasso.get()
                .load("https://www.gravatar.com/avatar/${md5Hex(emailUser)}")
                .into(avatar)
    }

    fun gravatarImageUrl(emailUser: String): String {
        return "https://www.gravatar.com/avatar/${md5Hex(emailUser)}"
    }

    private fun md5Hex(message: String): String? {
        var icon: String? = null
        val mdEnc: MessageDigest
        try {
            mdEnc = MessageDigest.getInstance("MD5")
            mdEnc.update(message.toByteArray(), 0, message.length)

            icon = BigInteger(1, mdEnc.digest()).toString(16)
        } catch (e1: NoSuchAlgorithmException) {
            e1.printStackTrace()
        }

        return icon
    }
}
