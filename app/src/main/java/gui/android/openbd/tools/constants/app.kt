package gui.android.openbd.tools.constants

/**
 * Created by jeremy on 07/04/18.
 *
 * Constant for generic data in application
 */
const val APP_VISIBILITY_SCROLL_BAR: Int = 15