package gui.android.openbd.tools

/**
 * Created by jeremy on 16/01/18.
 *
 * Transform string isbn with a good format
 */
class ISBN {
    companion object {
        fun transformIsbn13(isbn: String): String {
            var isbn13: String = isbn.substring(0, 3) + "-"

            isbn13 += isbn.substring(3, 4) + "-"
            isbn13 += isbn.substring(4, 8) + "-"
            isbn13 += isbn.substring(8, 12) + "-"
            isbn13 += isbn.substring(12, 13)

            return isbn13
        }

        fun transformIsbn10(isbn: String): String {
            var isbn10: String = isbn.substring(0, 1) + "-"

            isbn10 += isbn.substring(1, 3) + "-"
            isbn10 += isbn.substring(3, 9)

            return isbn10
        }
    }
}