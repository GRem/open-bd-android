package gui.android.openbd.tools.constants

/**
 * Created by jeremy on 17/01/18.
 *
 * Constants for Search
 */
const val SEARCH_SUCCESS: String =  "success"
const val SEARCH_FAILED: String =   "failed"
const val SEARCH_PRESENT: String =  "already"
