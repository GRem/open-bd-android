package gui.android.openbd.tools

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.widget.ImageView

/**
 * Created by jeremy on 14/01/18.
 *
 * Download image to API
 */
class DownloadImage(image: ImageView) : AsyncTask<String, Void, Bitmap>() {
    @SuppressLint("StaticFieldLeak")
    private var vImage: ImageView = image

    override fun doInBackground(vararg urls: String?): Bitmap {
        val urlDisplay = urls[0]
        val mIcon11: Bitmap

        val stream = java.net.URL(urlDisplay).openStream()
        mIcon11 = BitmapFactory.decodeStream(stream)

        return mIcon11
    }

    override fun onPostExecute(result: Bitmap) {
        vImage.setImageBitmap(result)
    }
}
