package gui.android.openbd

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

/**
 * Created by jeremy on 18/01/18.
 *
 * First class launching in application.
 */
class OpenBD: Application() {
    override fun onCreate() {
        super.onCreate()

        OpenBD.context = applicationContext
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        @JvmField
        var context: Context? = null

        // Not really needed since we can access the variable directly.
        @JvmStatic fun getOpenBDContext(): Context? {
            return context
        }
    }
}
