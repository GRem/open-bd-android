#!/bin/ash
#
# Sign APK generated

ANDROID_BIN=${ANDROID_HOME}/build-tools/${BUILD_TOOLS}/

set +x
mkdir output
cp -r app/build/outputs/apk/production/release/*.apk output/unsigned.apk
${ANDROID_BIN}/zipalign -v -p 4 output/unsigned.apk output/aligned.apk

${ANDROID_BIN}/apksigner sign \
  --v1-signing-enabled false \
  --v2-signing-enabled true \
  --ks app/open-bd-android.jks \
  --ks-key-alias ${OPENBD_ANDROID_KEY_ALIAS} \
  --ks-pass env:OPENBD_ANDROID_KEY_PASSWORD \
  --key-pass env:OPENBD_ANDROID_KEY_ALIAS_PASSWORD \
  --out output/open-bd.apk \
  output/aligned.apk
# ${ANDROID_BIN}/apksigner verify output/open-bd.apk
set -x
