#!/bin/ash
#
# Replace env variables to gradle properties file
#
# You may want another output file then use PROPERTIES_FILE_PATH variable for that
#
# Note: you can use System.getenv("VAR_NAME") instead of this if you dont need to create a local .properties files
# in your project.

PROPERTIES_FILE_PATH=gradle.properties
KEYSTORE_FILE_PATH=app/keystore.properties

set +x // dont print the next lines on run script

echo 'Configure endpoint'
sed -i -e "s/OPENBD_ANDROID_ENDPOINT=.*/OPENBD_ANDROID_ENDPOINT=$OPENBD_ANDROID_ENDPOINT/g" $PROPERTIES_FILE_PATH

echo 'Configure protocol'
sed -i -e "s/OPENBD_ANDROID_PROTOCOL=.*/OPENBD_ANDROID_PROTOCOL=$OPENBD_ANDROID_PROTOCOL/g" $PROPERTIES_FILE_PATH

echo 'Configure Client ID'
sed -i -e "s/OPENBD_ANDROID_CLIENT_ID=.*/OPENBD_ANDROID_CLIENT_ID=$OPENBD_ANDROID_CLIENT_ID/g" $PROPERTIES_FILE_PATH

echo 'Configure Client secret'
sed -i -e "s/OPENBD_ANDROID_CLIENT_SECRET=.*/OPENBD_ANDROID_CLIENT_SECRET=$OPENBD_ANDROID_CLIENT_SECRET/g" $PROPERTIES_FILE_PATH

echo 'Configure web access'
sed -i -e "s/OPENBD_ANDROID_WEB=.*/OPENBD_ANDROID_WEB=$OPENBD_ANDROID_WEB/g" $PROPERTIES_FILE_PATH

echo 'Configure store password'
sed -i -e "s/storePassword=.*/storePassword=$OPENBD_ANDROID_KEY_PASSWORD/g" $KEYSTORE_FILE_PATH

echo 'Configure key password'
sed -i -e "s/keyPassword=.*/keyPassword=$OPENBD_ANDROID_KEY_ALIAS_PASSWORD/g" $KEYSTORE_FILE_PATH

echo 'Configure key alias'
sed -i -e "s/keyAlias=.*/keyAlias=$OPENBD_ANDROID_KEY_ALIAS/g" $KEYSTORE_FILE_PATH

echo 'Configure key file'
sed -i -e "s/storeFile=.*/storeFile=$OPENBD_ANDROID_KEY_PATH/g" $KEYSTORE_FILE_PATH

curl -H "Private-Token: ${GITLAB_TOKEN}" $GITLAB_ANDROID_KEY --output "app/${OPENBD_ANDROID_KEY_PATH}"

set -x
